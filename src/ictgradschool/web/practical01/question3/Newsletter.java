package ictgradschool.web.practical01.question3;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * Servlet implementation class RegisterUser
 */
public class Newsletter extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static int COMPLETED_FORM_COUNT = 3;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Newsletter() {
        super();
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter out = response.getWriter();

        out.println("<!doctype html>");
        out.println("<html>");
        out.println("<head>");
        out.println("<title>Register for ICT GradSchool Newsletter</title>");
        out.println("<meta charset='UTF-8'>");
        out.println("</head>");
        out.println("<body>");

        HttpSession session = request.getSession();

        Enumeration<String> params = request.getParameterNames();
        if (params.hasMoreElements()) {
            // This is the result of either a submit form, or clear form

            if (request.getParameter("cleardata") != null) {

                Enumeration<String> attrs = session.getAttributeNames();
                while (attrs.hasMoreElements()){
                    String attr = attrs.nextElement();
                    session.removeAttribute(attr);
                }
                createForm(session, out);

            } else {
                int numFieldsFilledIn = storeFormDataSession(request);

                if (numFieldsFilledIn == 0) {
                    out.println("<p>No information entered.</p>");
                    out.println("<p>To return to the registration page, click <a href='./question03'>here</a>.</p>");
                } else if (numFieldsFilledIn != COMPLETED_FORM_COUNT) {
                    out.println("<p>The data you've entered so far has been saved.</p>");
                    out.println("<p>To continue your registration click <a href='./question03'>here</a>.</p>");
                } else {
                    out.println("<p>You have been registered!</p>");
                }
            }
        } else {
            createForm(session, out);
        }


        // close off the HTML page
        out.println("</body>");
        out.println("</html>");
    }


    protected int storeFormDataSession(HttpServletRequest request) {
        int count = 0;

        HttpSession sess = request.getSession(true);

        String fname;
        if( (fname= request.getParameter("fname"))!="") {
            sess.setAttribute("fname", fname);
            count++;
        }


        String lname;
        if((lname = request.getParameter("lname"))!= "") {
            sess.setAttribute("lname", lname);
            count++;
        }

        String email;
        if((email= request.getParameter("email"))!= "") {
            sess.setAttribute("email", email);
            count++;
        }

        return count;

    }

    protected Map<String, String> getFormDataSession(HttpSession session) {
        Map<String, String> userData = new HashMap<>();


       Enumeration<String> attrs = session.getAttributeNames();
       while (attrs.hasMoreElements()){
           String attr = attrs.nextElement();
           userData.put(attr, session.getAttribute(attr).toString());
       }
System.out.println(userData);
        return userData;
    }


    protected void createForm(HttpSession session, PrintWriter out) {

        Map<String, String> formFields = this.getFormDataSession(session);

        out.println("<form style='width:500px;margin:auto;' id='userform_id' name='userform' method='get' action='./question03'>");


        out.println("<fieldset><legend>Register for the ICT GradSchool Newsletter:</legend>");




        if(formFields.containsKey("fname")){
            out.println("<label for=\"fname\">First name:</label></br><input type=\"text\" name=\"fname\" id=\"fname\" size=\"40\" value=\"" + formFields.get("fname") +"\"><br><br>");
        } else {
            out.println("<label for=\"fname\">First name:</label></br><input type=\"text\" name=\"fname\" id=\"fname\" size=\"40\"><br><br>");
        }

        if(formFields.containsKey("lname")){
            out.println("<label for=\"lname\">Last name (no digits):</label></br><input type=\"text\" name=\"lname\" id=\"lname\" size=\"40\" value=\"" + formFields.get("lname") +"\"><br><br>");
        }else {
            out.println("<label for=\"lname\">Last name (no digits):</label></br><input type=\"text\" name=\"lname\" id=\"lname\" size=\"40\"><br><br>");
        }

        if (formFields.containsKey("email")){
            out.println("<label for=\"email\">Your email:</label></br><input type=\"text\" name=\"email\" id=\"email\" size=\"40\" value=\"" + formFields.get("email") +"\"><br><br>");
        } else {
            out.println("<label for=\"email\">Your email:</label></br><input type=\"text\" name=\"email\" id=\"email\" size=\"40\"><br><br>");
        }



        out.println("<input type='submit' name='submit_button' id='submit_id' value='Register' /></p>");

        out.println("</fieldset>");
        out.println("</form>");

        out.println("<form  style='width:500px;margin:auto;' id='clearform_id' name='clearform' method='get' action='./question03'>");
        out.println("<input type='hidden' name='cleardata' id='cleardata_id' value='clear' /></p>");
        out.println("<input type='submit' name='clear_button' id='clear_id' value='Clear Fields'></p>");

        out.println("</form>");
    }


    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}